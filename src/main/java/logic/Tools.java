/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logic;

/**
 *
 * @author manel
 */
public class Tools {
    
    // function that rotates s towards left by d
    public static String leftrotate(String str, int d)
    {
            String ans = str.substring(d) + str.substring(0, d);
            return ans;
    }
 
    // function that rotates s towards right by d
    public static String rightrotate(String str, int d)
    {
            return leftrotate(str, str.length() - d);
    }
    
}
