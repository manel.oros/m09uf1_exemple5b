package logic;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
*
* @author manel
* 
* Intenta trencar un hash MD5 generat en base a un string de longitud n generat a partir
* d'un alfabet de longitud m
* El màxim d'iteracións és longitud(m)^longitud(n)
* 
* Exemples:
* 5d59941e351dd0a040383590a3ee4f26 = 08374656
* 4d186321c1a7f0f354b297e8914ab240 = hola
* 6e6e2ddb6346ce143d19d79b3358c16a = adios
* 83f07f109264706409c1422f44e60d47 = @d10s
* ea9812873a40214b94a44e8d80805ea1 = m@n3l
* 5f4dcc3b5aa765d61d8327deb882cf99 = password
* 161ebd7d45089b3446ee4e0d86dbcf92 = P@ssw0rd
* 
*/
public class BruteForceHashCrack implements Runnable {

    /***
     * Llista d'observadors a l'esdeveniment EUREKA
     */
    private final List<EurekaListener> observers = new ArrayList<>();
    
    /***
    *  Alfabet de caràcters sobre els que permutar de forma exhaustiva.
    *
    * Exemples: 
    * "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\\\|;:\\'\\\",<.>/?"
    * "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    * "abcdefghijklmnopqrstuvwxyz0123456789"
    * "0123456789"
     */
    private final String charSet;
    
    /***
     * hash a petar
     */
    String hashToCrack;
    
    /***
     * iteració
     */
    String iteracio = "";
    
    /***
     * longitud exacta de la cadena a trobar. 
     * Si la cadena a trobar és menor, la ignora.
     */
    int maxParaula;
    
    /***
     * les iteracions que han sigut necessàries
     */
    long n_iterations = 0;
    
    /***
     * per establir els milisegons que triga el procès
     */
    long current = 0;
    long timeElapsed = 0;
    
    /***
     * string resultant, si l'ha trobat
     */
    String resultString = null;
    
    /***
     * HASH a utilitzar
     */
    MessageDigest md;
    
    /***
     * si true, finalitza el procès
     */
    boolean isStop = false;
    
    /***
     * si true, pausa el procès. Si false, el continua.
     */
    boolean isPaused = false;
    
    @Override
    public void run() {
        
        //posem crono a zero
        current = System.currentTimeMillis();
        
        // iniciem el procès
        this.genera(this.charSet, "", this.maxParaula);

    }

    /***
     * Constructor
     * 
     * @param _charSet Alfabet 
     * @param _hashToCrack Hash a trobar
     * @param max longitud fixa de la paraula a trencar. Ull, no verifica longituds menors!
     * @throws java.security.NoSuchAlgorithmException 
     */
    public BruteForceHashCrack(String _charSet, String _hashToCrack, int max) throws NoSuchAlgorithmException {
        
        this.hashToCrack = _hashToCrack;
        this.charSet = _charSet;
        
        md = MessageDigest.getInstance("MD5");
        
        this.maxParaula = max;
    }
    
    /***
     * Genera totes les mostres ordenades amb repetició sobre un conjunt de símbols.
     * 
     * @param elements Alfabet de caràcters sobre els que permutar
     * @param actual Element actual, necessàri per a la recursivitat
     * @param longitudParaula Longitud de la paraula a iterar
     */
    private void genera(String elements, String actual, int longitudParaula)
    {
        
        //si hem completat els caràcters de la combinació
        if (longitudParaula == 0){

            //acumulador 
            this.n_iterations++;

            //actualitzem iteracio
            this.iteracio = actual;

            //genera un hash en base a la combinació actual de caracters
            byte[] digest = md.digest(actual.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            //si el hash és igual al que estem buscant, és que hem trobat la combinació
            if (hashToCrack.equals(sb.toString()))
            {
                this.resultString = actual;

                // info de temps de procès
                this.timeElapsed = System.currentTimeMillis() - this.current;

                //llença l'esdeveniment 
                fireEureka();
                
            }
        }else{

            for (int i = 0; i < elements.length() && !this.isStop; i++){ 

               try {
                    while (this.isPaused) Thread.sleep (250);
               } catch (InterruptedException interruptedException) {
                   System.out.println("ERROR: " + interruptedException);
               }

               genera (elements, actual + elements.charAt(i), longitudParaula-1);
            }
        }
    }
    
    /***
     * Força la finalització "trencant" el procès
     */
    public void endTask()
    {
        this.isStop = true;
    }
    
    
    public String getResultString() {
        return resultString;
    }
    
    public String getHashToCrack() {
        return hashToCrack;
    }
    
    public long getIterations() {
        return n_iterations;
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }
    
    public String getIteracio() {
        return iteracio;
    }
    
    /***
     * Retorna un nombre entre 0 i 1 que indica el progrès
     * @return 
     */
    public Double getProgress() {
        
        return (this.n_iterations / Math.pow((double)charSet.length(), (double)maxParaula));
    }
    
    /***
     * Afegeix l'observer a l'eurekaFired
     * @param obs 
     */
    public void addObserver(EurekaListener obs) {
        this.observers.add(obs);
    }

    /***
     * Treu l'observer de l'eurekaFired
     * @param obs 
     */
    public void removeObserver(EurekaListener obs) {
        this.observers.remove(obs);
    }
    
    /***
     * Notifica als observers
     */
    private void fireEureka()
    {
        for (EurekaListener obs : this.observers)
        {
            obs.eurekaFired(this);
        }
    }
    
    public boolean isIsPaused() {
        return isPaused;
    }

    public void setIsPaused(boolean p) {
       
        this.isPaused = p;
    }
    
    /***
     * Per afegir listeners a l'esdeveniment de resultat trobat
     */
    public interface EurekaListener{
        
        // esdeveniment que es llença quan es troba un resultat
        public void eurekaFired(Object o);
        
    }
}
