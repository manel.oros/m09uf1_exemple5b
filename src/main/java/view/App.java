package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Senzilla aplicació multifil JavaFX que permet rencar varis un hash MD5 simultàniament
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static PrimaryController controller;

    @Override
    public void start(Stage stage) throws IOException {
        // fa que no es pugui maximitzar
        stage.setResizable(false);
        
        scene = new Scene(loadFXML("primary"));
        
        //apliquem els estils
        scene.getStylesheets().add(getClass().getResource("css/primer-dark.css").toExternalForm());
        
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        Parent c = fxmlLoader.load();
        controller = fxmlLoader.<PrimaryController>getController();
        return c;
    }

    public static void main(String[] args) {
        launch();
    }
    
    @Override
    public void stop() throws Exception {
        //mata tots els processos en marxa abans de tancar
        controller.eurekaFired(this);
    }
}