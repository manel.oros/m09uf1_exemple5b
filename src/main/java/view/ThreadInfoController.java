/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import logic.BruteForceHashCrack;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class ThreadInfoController implements Runnable {

    @FXML
    private ProgressBar progressBar;
    @FXML
    public Label lbl_pid;
    @FXML
    public Label lbl_try;
    @FXML
    private Label lbl_timeElapsed;
    @FXML
    private Label lbl_iterations;
    @FXML
    private Label lbl_result;
    @FXML
    private Button btn_PausePlay;
    @FXML
    private ScrollPane scrollPane1;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private FlowPane flow_Pane;
    
    
    //tasca a realitzar
    private BruteForceHashCrack tasca;
    
    Thread fil;
    
    @FXML
    void onAction_btn_Pause(ActionEvent event) {
        
        if (tasca.isIsPaused())
        {
            this.tasca.setIsPaused(false);
            this.btn_PausePlay.setText("Pause");
        }
        else
        {
            this.tasca.setIsPaused(true);
            this.btn_PausePlay.setText("Resume");
        }
    }

    @FXML
    private void onAction_btn_Cancel(ActionEvent event) {
        
       this.tasca.endTask();
       hBoxButtons.setDisable(true);
       
    }
    
    public void setTasca(BruteForceHashCrack tasca) {
        this.tasca = tasca;
    }

    public BruteForceHashCrack getTasca() {
        return tasca;
    }
    
    @Override
    public void run() {
        
        //habilita l'execució de la tasca com a fil
        UpdateVisualTask uvt = new UpdateVisualTask();
        
        fil = new Thread(uvt);

        //iniciem el fil
        fil.start();

        // s' inicia
        this.tasca.run();

        //parem fil de visualització
        uvt.setIsStop(true);
    }
    
    /***
     * Actualitza la visualització cada 100 milisegons
     */
    class UpdateVisualTask implements Runnable{
        
        boolean isStop = false;

        public void setIsStop(boolean isStop) {
            this.isStop = isStop;
        }

        @Override
        public void run() {
            
            while (!isStop){
                try {
                    //frecuencia de refresh
                    Thread.sleep(100);
                    Platform.runLater(() -> {
                        updateVisual();
                    });
                } catch (InterruptedException ex) {
                    System.out.println("Error actualitzant visualitzacio: " + ex.toString());
                }
            }
        }
        
        /***
        * Actualitza la part visual del ThreadInfoController
        */
        private void updateVisual()
        {
            
            lbl_timeElapsed.setText(Long.toString(tasca.getTimeElapsed()));
            lbl_iterations.setText(Long.toString(tasca.getIterations()));
            lbl_result.setText(tasca.getResultString()==null?"----":tasca.getResultString());
            lbl_try.setText(tasca.getIteracio());
            progressBar.setProgress(tasca.getProgress());
            if (tasca.getResultString() != null)
                scrollPane1.setStyle("-fx-border-color: lime;");
        }
    }
}
