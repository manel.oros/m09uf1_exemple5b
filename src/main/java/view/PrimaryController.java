/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import logic.BruteForceHashCrack;
import logic.Tools;
import logic.BruteForceHashCrack.EurekaListener;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class PrimaryController implements EurekaListener, Initializable{
    
    /***
     * Llista d'objectes-fils
     */
    List<BruteForceHashCrack> crackersList = new ArrayList<>();
    
    private final String[] charSets = {"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\\\\\\\|;:\\\\'\\\\\\\",<.>/?"
                                       ,"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
                                       ,"abcdefghijklmnopqrstuvwxyz0123456789"
                                       ,"0123456789"};

    
    private String charSetSelected;
    
    @FXML
    private TextField txt_hash_to_break;
    
    @FXML
    private FlowPane flow_Pane;
    
    // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\\\|;:\\'\\\",<.>/?
    @FXML
    private RadioButton radio1;

    // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
    @FXML
    private RadioButton radio2;
    
    //abcdefghijklmnopqrstuvwxyz0123456789
    @FXML
    private RadioButton radio3;

    //0123456789
    @FXML
    private RadioButton radio4;
    
    @FXML
    private Slider threadsSlider;
    
    @FXML
    private Slider passwordSlider;
    
    @FXML
    private Label lbl_iterations;
    
    @FXML
    private ToggleGroup grupRadios;
    
    //nombre de fils seleccionats al slider
    Integer n_threads;
    
     //nombre de caràcters del password
    Integer n_caracters;
    
     @Override
    public void initialize(URL url, ResourceBundle rb) {
        
         createListenerControls();
         
         txt_hash_to_break.setText("4d186321c1a7f0f354b297e8914ab240");
         
         threadsSlider.setValue(Runtime.getRuntime().availableProcessors());
         
         passwordSlider.setValue(4d);
         
         radio3.setSelected(true);
    }

    @FXML
    private void onAction_btnCrack(ActionEvent event) {
       
        flow_Pane.getChildren().clear();

        //nombre de fils seleccionats al slider
        n_threads = (int)Math.round(threadsSlider.getValue());

        n_caracters = (int)Math.round(passwordSlider.getValue());
        
        try {

            //verifica que l'entrada tingui forma de hash
            if (this.txt_hash_to_break.getText().trim().matches("^[0-9a-fA-F]{32}$"))
            {
                // desplaçaments que hem de fer sobre l'alfabet per "tallar-lo" en tants fragments com threads seleccionats
                int offset = charSetSelected.length() / n_threads;

                //estableix el nombre màxim de fils
                for (int i = 0; i < n_threads; i++)
                {

                    String currentCharSet = Tools.rightrotate(charSetSelected, offset*i);


                    FXMLLoader loader = new FXMLLoader(getClass().getResource("threadInfo.fxml"));

                    //just en aquest punt s'executa el metode initialize del ThreadInfoController
                    flow_Pane.getChildren().add(loader.load());
                    ThreadInfoController c = loader.getController();

                    //li "injectem" la tasca a realitzar
                    BruteForceHashCrack cr =new BruteForceHashCrack(currentCharSet, txt_hash_to_break.getText(), n_caracters);
                    cr.addObserver(this);
                    crackersList.add(cr);
                    c.setTasca(cr);

                    //habilita l'execució de la tasca com a fil
                    Thread fil = new Thread(c);

                    //mostrem el thread ID
                    c.lbl_pid.setText(String.valueOf(fil.getId()));

                    //iniciem el fil
                    fil.start();

                }
            }
        } catch (IOException ex1) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex1);
        } catch (NoSuchAlgorithmException ex2) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error amb el HASH");
            alert.setContentText(ex2.toString());
            alert.showAndWait();
        }
    }

    @Override
    public void eurekaFired(Object o) {
        
        for (BruteForceHashCrack cr : crackersList)
            cr.endTask();
    }
    
    /***
     * Actualitza la vista
     * 
     */
    private void updateView()
    {
       // No cal el else perquè els if són excloents de forma implícita perquè els radiobutton 
       // estan agrupats en un ToggleGroup i mai es poden seleccionar dos alhora.
       if (radio1.isSelected()) 
           charSetSelected = charSets[0];
       if (radio2.isSelected()) 
           charSetSelected = charSets[1];
       if (radio3.isSelected()) 
           charSetSelected = charSets[2];
       if (radio4.isSelected()) 
           charSetSelected = charSets[3];
     
       n_threads = (int)Math.round(threadsSlider.getValue());
       
       n_caracters = (int)Math.round(passwordSlider.getValue());
       
       //calcula el màxim d'iteracions en funció de la selecció
       Double d = Math.pow(charSetSelected.length(), n_caracters);
       String str = String.format("%,.0f", d);
       lbl_iterations.setText(str);
    }
    
    /***
     * Habilita l'actualització dels indicadors cada cop que es modifiquen els sliders
     */
    private void createListenerControls()
    {
        threadsSlider.valueProperty().addListener((ObservableValue <?extends Number>observable, Number oldValue, Number newValue) -> {
            updateView();
        });
        
        passwordSlider.valueProperty().addListener((ObservableValue <?extends Number>observable, Number oldValue, Number newValue) -> {
            updateView();
        });
               
        grupRadios.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
            updateView();
        });
    }

   
}
